<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <style>
      body {
		color: #495057;
		font-size: 13px;
		background-color: #f8f8f8;
		font-family: open sans,tahoma,sans-serif;
      }
	  
	  .form-control {
		  font-size:13px;
	  }
	  
      @media (min-width: 992px) {
        body {
          padding-top: 64px;
        }
      }
	  
	  .container-fluid {
		  margin-top: -10px;
	  }
	  
	  .bg-toped{
		  background: #f3f3f3;
		  border-bottom: 1px solid #d9d9d9;
	  }
	  
	  .navbar-brand{
		font-size: 1.45rem;
		font-family: 'Indie Flower', cursive;
	  }
	  
	  .navbar-light .navbar-brand {
		  color: #42b549!important;
	  }
	  
	  .navbar-light .navbar-nav .nav-link {
		  color: #606060;
	  }
	  
	  .dropdown-item {
		  font-size: 12px;
		  color: #495057;
	  }
	  
	  .bread {
		  background-color: #f8f8f8;
	  }
	  
	  .spasi {
		  padding: 10px;
	  }
	  
	  .mb-5 {
		  margin-bottom: 5px!important;
	  }
	  
	  .fs-11 {
		  font-size: 11px!important;
	  }
		
	  a {
		  color: #42b549;
		  text-decoration: none;
		  cursor: pointer;
	  }
	  
	  a:hover, a:focus {		  
		  color: #ff5722;
		  text-decoration: none;
	  }
	  
	  a:hover, a:active {
		  outline: 0;
		  cursor: pointer;
	  }
	  
	  .card-header {
		  padding-top: 0.75rem;
		  padding-right: 1.25rem;
		  padding-bottom: 0.75rem;
		  padding-left: 0.5rem;
		  margin-bottom: 0;
		  background-color: #fff;
		  border-bottom: 1px solid rgba(0,0,0,.125);
	  }
	  
	  .card-body {
		  -ms-flex: 1 1 auto;
		  flex: 1 1 auto;
		  padding: 0.5rem;
	  }
	  
	  .card-header{
		  font-weight: 640;
	  }
	  
	  .card-title {
		  font-size: 16px;
		  margin-bottom: .75rem;
	  }
	  
	  .search {
		  background-color: #fff;
		  border-top-color: rgba(206, 212, 218, 1);
		  border-right-color: rgb(255, 255, 255);
		  border-bottom-color: rgba(206, 212, 218, 1);
		  border-left-color: rgba(206, 212, 218, 1);
	  }
	  
	  .input-search{
		  border-left-color: rgb(255, 255, 255);
		  border-left-style: solid;
		  border-left-width: 1px;
		  font-size: 13px;
	  }
	  
	  .input-search:focus {
		  border-top-color: rgb(206, 212, 218) !important;
		  border-right-color: #ff5722;
		  border-bottom-color: rgb(206, 212, 218) !important;
		  border-left-color: rgb(255, 255, 255);
		  box-shadow: 0 0 0 0rem;
	  }
	  
	  .btn-search {
		  background-color: #ff5722;
		  border-color: #ff5722;
		  color: #fff;
		  font-size: 13px;
	  }
	  
	  .btn-search:focus{
		  border-color: #ff5722;
		  box-shadow: 0 0 0 0.2rem rgba(255, 87, 36, 0.60) !important;
	  }
	  
	  .btn-sm {
		  font-size: 13px;
	  }
	  
	  .green:focus {
		  border-color:#28a745!important;
		  box-shadow: 0 0 0 0.2rem rgba(65, 180, 73, 0.49) !important;
	  }
	  

    </style>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-toped fixed-top">
      
        <a class="navbar-brand" href="#">YukDiOrder</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button><div class="container">
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="/">Beranda
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php/barang">Barang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php/pelanggan">Pelanggan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php/penjualan">Penjualan</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="index.php/pembelian">Pembelian</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="index.php/resi">Resi</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="index.php/cek_ongkir">Cek Ongkir</a>
            </li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				  Pengaturan
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				  <a class="dropdown-item" href="#">Pengguna</a>
				  <a class="dropdown-item" href="#">Another action</a>
				  <a class="dropdown-item" href="#">Something else</a>
				  <a class="dropdown-item" href="#">Log Out</a>
				</div>
			  </li>
          </ul>
        </div>
      </div>
    </nav>
	
	<!-- Breadcrumb -->
	<div class="col-md-12">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb bread">
			<li class="breadcrumb-item active" aria-current="page"> Beranda / Data Barang</li>
		  </ol>
		</nav>
	</div>
	
	
    <!-- Page Content -->
	
	<div id="content">
	<div class="container-fluid">
	<div class="col-xl-12">
	  <div class="row ">
		
		<div class="col-md-9">
			<div class="card">
			 
			 <div class="card-body">
				
				<h5 class="card-title">Data Barang</h5>
				<hr>
				
				<table class="table table-bordered">
					<thead>
					<tr>
					  <th>ID</th>
					  <th>Nama Barang</th>
					  <th>Kategori</th>
					  <th>Stok Minimum</th>
					  <th>Stok</th>
					  <th>Nilai Stok</th>
					  <th>Aksi</th>
					</tr>
				  </thead>
				  <tbody>
					<tr>
					 <td>1</td>
					 <td>Azalea Dress</td>
					 <td>Gamis</td>
					 <td>20</td>
					 <td>20</td>
					 <td>50</td>
					 
					 <td>
						<a href="#"><i class="fa fa-search fa-fw"></i>Lihat</a>
						<a href="#"><i class="fa fa-pencil fa-fw"></i>Edit</a>
						<a href="#"><i class="fa fa-trash fa-fw"></i>Hapus</a>
					  </td>
				  </tbody>
				</table>
			  </div>
			</div>
			<div class="spasi"></div>
		</div>
		
		
		
		<div class="col-md-3">
			<div class="card mb-3">
				<div class="card-header">Cari Barang</div>
				<div class="card-body">
				  <form>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<div class="input-group-text search"><i class="fa fa-search"></i></div>
						</div>
					  <input type="text" class="form-control input-search">
					  <div class="input-group-append">
						<input type="submit" class="btn btn-search" value="Cari">
					  </div>
					</div>
					<div class="form-group ">
				  <form>
				</div>
			</div>
			</div>
			
			<div class="card mb-3">
				<div class="card-header">Input Barang</div>
				<div class="card-body">
				 <form>
				  <div class="form-group row">
					<label class="col-sm-5 col-form-label">Nama Barang</label>
					<div class="col-sm-7">
					  <input type="text" class="form-control form-control-sm green">
					</div>
				  </div>
				  
				  <div class="form-group row">
					<label class="col-sm-5 col-form-label">Kategori</label>
					<div class="col-sm-7">
					  <input type="text" class="form-control form-control-sm green">
					</div>
				  </div>
				  
				  <div class="form-group row">
					<label class="col-sm-5 col-form-label">Stok</label>
					<div class="col-sm-7">
					  <input type="text" class="form-control form-control-sm green">
					</div>
				  </div>
				  
				  <div class="form-group row">
					<label class="col-sm-5 col-form-label">Nilai Stok</label>
					<div class="col-sm-7">
					  <input type="text" class="form-control form-control-sm green">
					</div>
				  </div>
				  
				  <input type="submit" class="btn btn-success btn-sm pull-right " value="Simpan">
				</form>
				</div>
			</div>
			
			<div class="card mb-3">
				<div class="card-header">Input Attribut</div>
				<div class="card-body">
				 <form>
				  <div class="form-group row">
					<label class="col-sm-5 col-form-label">Ukuran</label>
					<div class="col-sm-7">
					  <input type="text" class="form-control form-control-sm green">
					</div>
				  </div>
				  
				  <div class="form-group row">
					<label class="col-sm-5 col-form-label">Warna</label>
					<div class="col-sm-7">
					  <input type="text" class="form-control form-control-sm green">
					</div>
				  </div>
				  <input type="submit" class="btn btn-success btn-sm pull-right " value="Simpan">
				</form>
				</div>
			</div>
			
			<div class="card mb-3">
				<div class="card-header">Barang Terlaris</div>
				<div class="card-body">	
					<table class="table table-bordered">
						<thead>
						<tr>
						  <th>#</th>
						  <th>First</th>
						  <th>Last</th>
						</tr>
					  </thead>
					  <tbody>
						<tr>
						  <th>1</th>
						  <td>Mark</td>
						  <td>Otto</td>
						</tr>
						<tr>
						  <th>1</th>
						  <td>Mark</td>
						  <td>Otto</td>
						</tr>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
			
			
			
        </div>
      </div>
	  
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
