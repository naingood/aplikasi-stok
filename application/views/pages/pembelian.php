<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <style>
      body {
		color: #495057;
		font-size: 13px;
		background-color: #f8f8f8;
		font-family: open sans,tahoma,sans-serif;
      }
	  
	  .form-control {
		  font-size:13px;
	  }
	  
      @media (min-width: 992px) {
        body {
          padding-top: 64px;
        }
      }
	  
	  .container-fluid {
		  margin-top: -10px;
	  }
	  
	  .bg-toped{
		  background: #f3f3f3;
		  border-bottom: 1px solid #d9d9d9;
	  }
	  
	  .navbar-brand{
		font-size: 1.45rem;
		font-family: 'Indie Flower', cursive;
	  }
	  
	  .navbar-light .navbar-brand {
		  color: #42b549!important;
	  }
	  
	  .navbar-light .navbar-nav .nav-link {
		  color: #606060;
	  }
	  
	  .dropdown-item {
		  font-size: 12px;
		  color: #495057;
	  }
	  
	  .bread {
		  background-color: #f8f8f8;
	  }
	  
	  .spasi {
		  padding: 10px;
	  }
	  
	  .mb-5 {
		  margin-bottom: 5px!important;
	  }
	  
	  .fs-11 {
		  font-size: 11px!important;
	  }
		
	  a {
		  color: #42b549;
		  text-decoration: none;
		  cursor: pointer;
	  }
	  
	  a:hover, a:focus {		  
		  color: #ff5722;
		  text-decoration: none;
	  }
	  
	  a:hover, a:active {
		  outline: 0;
		  cursor: pointer;
	  }
	  
	  .card-header {
		  padding-top: 0.75rem;
		  padding-right: 1.25rem;
		  padding-bottom: 0.75rem;
		  padding-left: 0.5rem;
		  margin-bottom: 0;
		  background-color: #fff;
		  border-bottom: 1px solid rgba(0,0,0,.125);
	  }
	  
	  .card-body {
		  -ms-flex: 1 1 auto;
		  flex: 1 1 auto;
		  padding: 0.5rem;
	  }
	  
	  .card-header{
		  font-weight: 640;
	  }
	  
	  .card-title {
		  font-size: 16px;
		  margin-bottom: .75rem;
	  }
	  
	  .search {
		  background-color: #fff;
		  border-top-color: rgba(206, 212, 218, 1);
		  border-right-color: rgb(255, 255, 255);
		  border-bottom-color: rgba(206, 212, 218, 1);
		  border-left-color: rgba(206, 212, 218, 1);
	  }
	  
	  .input-search{
		  border-left-color: rgb(255, 255, 255);
		  border-left-style: solid;
		  border-left-width: 1px;
		  font-size: 13px;
	  }
	  
	  .input-search:focus {
		  border-top-color: rgb(206, 212, 218) !important;
		  border-right-color: #ff5722;
		  border-bottom-color: rgb(206, 212, 218) !important;
		  border-left-color: rgb(255, 255, 255);
		  box-shadow: 0 0 0 0rem;
	  }
	  
	  .btn-search {
		  background-color: #ff5722;
		  border-color: #ff5722;
		  color: #fff;
		  font-size: 13px;
	  }
	  
	  .btn-search:focus{
		  border-color: #ff5722;
		  box-shadow: 0 0 0 0.2rem rgba(255, 87, 36, 0.60) !important;
	  }
	  
	  .btn-sm {
		  font-size: 13px;
	  }
	  
	  .green:focus {
		  border-color:#28a745!important;
		  box-shadow: 0 0 0 0.2rem rgba(65, 180, 73, 0.49) !important;
	  }
	  
	  .btn-orange-green{
		  color: #fff;
		  background-color: #28a745;
		  border-color: #28a745;
	  }
	  
	  .btn-orange-green:hover{
		  color: #fff;
		  background-color: #ff5722;
		  border-color: #da5227;
	  }
	  
	  .btn-orange-green:focus{
		  box-shadow: 0 0 0 0rem;
	  }
	  

    </style>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-toped fixed-top">
      
        <a class="navbar-brand" href="#">YukDiOrder</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button><div class="container">
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#">Beranda
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Barang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Pelanggan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Penjualan</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="#">Pembelian</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="#">Resi</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="#">Cek Ongkir</a>
            </li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				  Pengaturan
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				  <a class="dropdown-item" href="#">Pengguna</a>
				  <a class="dropdown-item" href="#">Another action</a>
				  <a class="dropdown-item" href="#">Something else</a>
				  <a class="dropdown-item" href="#">Log Out</a>
				</div>
			  </li>
          </ul>
        </div>
      </div>
    </nav>
	
	<!-- Breadcrumb -->
	<div class="col-md-12">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb bread">
			<li class="breadcrumb-item active" aria-current="page"> Beranda / Pembelian</li>
		  </ol>
		</nav>
	</div>
	
	
    <!-- Page Content -->
	
	<div id="content">
		<div class="container-fluid">
			<div class="col-xl-12">
				  <div class="row ">
						<div class="col-md-9">
	
							<div class="card">
							  <div class="card-body">
								<h5 class="card-title">Masukan Barang Pembelian</h5>
								<hr>
								<table class="table table-bordered">
									<thead>
									<tr>
										<th width="20%">ID Barang</th>
										<th width="20%">Nama Barang</th>
										<th width="20%">Ukuran</th>
										<th width="20%">Warna</th>
										<th width="20%">Jumlah</th>
									</tr>
								  </thead>
								  <tbody>
									<tr>
									 <form action='./' method='POST'>
										<td><input type='text' class='form-control form-control-sm' name='id' size="5"></td>
										<td><input type='text' class='form-control form-control-sm' name='nama'></td>
										<td><input type='text' class='form-control form-control-sm' name='ukuran'></td>
										<td><input type='text' class='form-control form-control-sm' name='warna'></td>
										<td><input type='text' class='form-control form-control-sm' name='jumlah'></td>
										
									  </tr>
									  <tr><td colspan='5'><input type='submit' class='btn btn-sm btn-success pull-right' name='submit' value='Tambah Barang'></td></tr>
									  </form>
									</tr>
								  </tbody>
								</table>
								<br>
								<h5 class="card-title">Rincian pembelian</h5>
								<hr>
								
								<table class="table table-bordered">
								<thead>
									<tr>
										<th>ID Barang</th>
										<th>Jumlah</th>
										<th>Nama Barang</th>
										<th>Ukuran</th>
										<th>Warna</th>
										<th>Harga</th>
										<th>Subtotal</th>
									</tr>
								  </thead>
								  <tbody>
									 <tr>
										<td>B001</td>
										<td>2</td>
										<td>Gamis 1</td>
										<td>M</td>
										<td>Hijau Tosca</td>
										<td>120.000</td>
										<td>240.000</td>
									  </tr>
									   <tr>
										<td>B001</td>
										<td>2</td>
										<td>Gamis 1</td>
										<td>M</td>
										<td>Hijau Tosca</td>
										<td>120.000</td>
										<td>240.000</td>
									  </tr>
										<tr>
										<td>B001</td>
										<td>2</td>
										<td>Gamis 1</td>
										<td>M</td>
										<td>Hijau Tosca</td>
										<td>120.000</td>
										<td>240.000</td>
									  </tr>
									  <form>
									  <tr><td colspan='4'></td><td colspan='2'>Subtotal</td><td>720.000</td></tr>
									  <tr><td colspan='4'></td><td>Diskon</td><td><input type='text' class='form-control form-control-sm' name='diskon' size="5"></td><td>(72.000)</td></tr>
									  <tr><td colspan='4'></td><td>PPh</td><td><input type='text' class='form-control form-control-sm' name='pajak' size="5"></td><td>(72.000)</td></tr>
									  <tr><td colspan='6'><h6>Total</h6></td><td>648.000</td><tr>
									  <tr><td colspan='7'><input type='submit' class='btn btn-success btn-sm pull-right' name='submit' value='Proses Keep'></td></tr>
									  
									  </form>
								  </tbody>
								</table>
								
							  </div>
							</div>
								
							</div>
							
							
							<div class="col-md-3">
								<div class="card mb-3">
									<div class="card-header">Penjualan Pelanggan</div>
									<div class="card-body">
										<table id="tabel" >
										<tr>
											<td>ID</td>
											<td>:</td>
											<td><input type='text' name='id' class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td>Nama</td>
											<td>:</td>
											<td>Isnainy Nasrullah</td>
										</tr>
										<tr>
											<td>Alamat</td>
											<td>:</td>
											<td>Perum Piranha Village No. 6, Tunjungsekar , Lowokwaru , Malang, 65142</td>
										</tr>
										<tr>
											<td>Telepon</td>
											<td>:</td>
											<td>085233337840</td>
										</tr>
										
										</table>
									</div>
								</div>
								
								<div class="card mb-3">
									<div class="card-header">Histori Belanja</div>
									<div class="card-body">	
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td><a href="#">01-05-18</a></td>
													<td>500.000</td>
													<td>30 hari lalu</td>
												</tr>
												<tr>
													<td><a href="#">15-05-18</a></td>
													<td>250.000</td>
													<td>15 hari lalu</td>
												</tr>
												<tr>
													<td><a href="#">29-05-18</a></td>
													<td>750.000</td>
													<td>1 hari lalu</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan='2'>Total Belanja</td>
													<td>1.000.000</td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
				  </div>
			</div>
		</div>
	</div>

    <!-- Bootstrap core JavaScript -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
