<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <style>
      body {
        padding-top: 54px;
		line-height: 1.7;
		color: #495057;
		font-size: 13px;
		background-color: #f8f8f8;
		font-family: open sans,tahoma,sans-serif;
      }
	  
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }
	  
	  .bg-toped{
		  background: #f3f3f3;
		  border-bottom: 1px solid #d9d9d9;
	  }
	  
	  .navbar-light .navbar-brand {
		  color: #42b549!important;
	  }
	  
	  .navbar-light .navbar-nav .nav-link {
		  color: #606060;
	  }
	  
	  .dropdown-item {
		  font-size: 12px;
		  color: #495057;
	  }
	  

    </style>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-toped fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">YukDiOrder</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Beranda
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Barang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Pelanggan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Penjualan</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="#">Pembelian</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="#">Resi</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="#">Cek Ongkir</a>
            </li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				  Pengaturan
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				  <a class="dropdown-item" href="#">Pengguna</a>
				  <a class="dropdown-item" href="#">Another action</a>
				  <a class="dropdown-item" href="#">Something else</a>
				  <a class="dropdown-item" href="#">Log Out</a>
				</div>
			  </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1 class="mt-5">A Bootstrap 4 Starter Template</h1>
          <p class="lead">Complete with pre-defined file paths and responsive navigation!</p>
          <ul class="list-unstyled">
            <li>Bootstrap 4.1.1</li>
            <li>jQuery 3.3.1</li>
          </ul>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
